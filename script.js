
const userUrl = "https://ajax.test-danit.com/api/json/users";
const postUrl = "https://ajax.test-danit.com/api/json/posts";
const deleteUrl = "https://ajax.test-danit.com/api/json/posts/";


class Card {
    constructor(name, username, email, title, text) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.title = title;
        this.text = text;
        this.generate();
        this.btns = document.getElementsByClassName("card__btn");
        this.btnFunc()
    }


    generate() {
       let element = document.createElement("div");
       element.classList.add("card")
       element.innerHTML = `
            <span class="card__name">${this.name}</span>
            <span class="card__username">${this.username}</span>
            <span class="card__email">${this.email}</span>
            <h2 class="card__title">${this.title}</h2>
            <p class="card__text">${this.text}</p> 
            <a href="#" class="card__btn">DELETE</a>  
       ` 
       container.append(element) 
    }

    btnFunc() {
        for(let i = 0; i<this.btns.length; i++){
            this.btns[i].addEventListener("click", e => {
                let id = Array.prototype.indexOf.call(this.btns, e.target)
                if(deletePost(id)) {
                    e.target.closest("div").style.display = 'none'
                }
            })
        }
        
    }
}

// ПОМЕНЯТЬ ВСТАВИТЬ КУДА ТО.
let container = document.createElement('div');
container.classList.add('container')  
document.body.append(container)



function getUserData() {
    return fetch(userUrl)   
}

function getPostData() {
    return fetch(postUrl)
}

function deletePost(postId) {
    let url = `https://ajax.test-danit.com/api/json/posts/${postId}`;

    let options = {
        method: "DELETE"
    }

    return fetch(url, options)
        .then(response => {
            response.ok
        })
      
}


getUserData().then((response) => response.json()).then((data) => {
    data.forEach(element => {
        getPostData().then((response) => response.json()).then((data) => {
            data.forEach(item => {
                if(element.id === item.userId){
                    new Card(element.name, element.username, element.email, item.title, item.body) 
                }
            })    
        })
    }); 
});





